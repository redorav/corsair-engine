#pragma once

#include <EASTL/array.h>

template<typename T, int N>
using CrArray = eastl::array<T, N>;