#pragma once

#include <EASTL/fixed_vector.h>

template<typename T, size_t N>
using CrFixedVector = eastl::fixed_vector<T, N>;