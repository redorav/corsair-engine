#pragma once

#include "EASTL/utility.h"

template<typename T, typename S>
using CrPair = eastl::pair<T, S>;