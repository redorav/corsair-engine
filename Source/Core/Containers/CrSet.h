#pragma once

#include <EASTL/set.h>

template<typename T>
using CrSet = eastl::set<T>;