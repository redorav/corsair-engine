#pragma once

#include <EASTL/vector.h>

template<typename T>
using CrVector = eastl::vector<T>;