#pragma once

namespace cr3d
{
	namespace DataFormat { enum T : uint32_t; }
	enum class TextureType : uint8_t;
	using TextureUsageFlags = uint32_t;
	enum class SampleCount :uint8_t;
	struct DataFormatInfo;
	namespace ShaderStage { enum T : uint32_t; }
}